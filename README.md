# event-sourcing-study

A study on event-sourcing

## Application Logic
This application imitates a delivery service. Orders are received, paid for, and then delivered.

The domain events are:
- Order received
- Payment received
- Shipment started
    - NOTE: each shipment is assigned a random 'runner_id' from 0 to 9
- Shipment delivered

And to show the power of event sourcing, we will build 2 independent UIs off of these events.
- Orders UI: Shows all orders, and which state each is currently in.
- Runners UI: Tracks all 10 runners. Including current active orders (not yet delivered), and completed orders (delivered) for each runner.

## Application Architecture
Components of this study include:
- A Redis server, used as an event message broker (via Redis queues)
- A producer application, producing domain events
- A consumer application, listening to domain events, and creating views for the UIs mentioned above
- A Postgres database server, used as the event store (TODO)
    - Current implementation uses an in-memory event store
- An angular application (TODO)




## Interaction with Related technologies

### Domain Driven Design
Event-sourcing works very well with Domain Driven design. Using domain language in your events makes them business specific, instead of technology specific.

This means your event stream does not change based on underlying technologies, but only on changes in response to business rules.

### Event Driven Architecture
This application utilizes redis streams for sending events from one application to another.

Event streaming is used to separate event sources from event consumers. This has a number of benefits, including: 
- Scaling consumer and producer apps independently
- Adding new consumer applications to existing streams (e.g. a v2 version of an existing app, or an entirely separate logging app)
- Fault tolerance in case either consumer or producer is unreliable - very common in IoT

Because event streaming already requires that you represent changes in your application as serializable events, event sourcing fits naturally into that environment.

It is worth mentioning that an event broker (like redis, rabbitMQ, kafka) is not a requirement for event sourcing. Event sourcing can be applied anywhere where entity changes can be represented by a series of serializable events. You could create events from an RESTful API and store them in a SQL server.