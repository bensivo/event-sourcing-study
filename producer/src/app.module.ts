import { Module } from '@nestjs/common';
import { OrdersModule } from './orders/orders.module';
import { ShipmentsModule } from './shipments/shipments.module';
import { RedisModule } from './redis/redis.module';
import { PaymentsModule } from './payments/payments.module';

@Module({
  imports: [
    OrdersModule,
    ShipmentsModule,
    RedisModule,
    PaymentsModule
  ],
})
export class AppModule { }
