import { Module } from '@nestjs/common';
import { RedisModule } from 'src/redis/redis.module';
import { ShipmentsService } from './shipments.service';

@Module({
  imports: [RedisModule],
  providers: [ShipmentsService]
})
export class ShipmentsModule {}
