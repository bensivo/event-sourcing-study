import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { RedisService } from 'src/redis/redis.service';

@Injectable()
export class ShipmentsService implements OnApplicationBootstrap{

    logger = new Logger(ShipmentsService.name)

    consumerId = 'shipments-' + Math.round(Math.random() * 1000);

    constructor(private redisService: RedisService) { }

    onApplicationBootstrap() {
        this.redisService.poll('payment_received', 'shipments', this.consumerId, this.onPaymentReceived.bind(this));
        this.redisService.poll('shipment_started', 'shipments', this.consumerId, this.onShipmentStarted.bind(this));
    }

    async onPaymentReceived(event: any) {
        await wait(Math.random() * 1000);

        this.logger.log(`Shipment started for order: ${event.event_payload.order_id}`);
        this.redisService.emit({
            event_name: 'shipment_started',
            event_ts: Date.now(),
            event_version: 1,
            event_payload: {
                order_id: event.event_payload.order_id,
                runner_id: Math.round(Math.random() * 10), // See README. Randomly assigns one of 10 runners. In our consumer, we will build an UI for tracking all runners.
            }
        });
    }

    async onShipmentStarted(event: any) {
        await wait(Math.random() * 1000);

        this.logger.log(`Shipment delivered for order: ${event.event_payload.order_id}`);
        this.redisService.emit({
            event_name: 'shipment_delivered',
            event_ts: Date.now(),
            event_version: 1,
            event_payload: {
                order_id: event.event_payload.order_id,
                runner_id: event.event_payload.runner_id,
            }
        });
    }
}

function wait(ms: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, ms));
}