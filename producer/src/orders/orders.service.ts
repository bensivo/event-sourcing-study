import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { Event } from 'src/events/event.interface';
import { RedisService } from 'src/redis/redis.service';
import * as uuid from 'uuid';


@Injectable()
export class OrdersService implements OnApplicationBootstrap {
    logger = new Logger(OrdersService.name)

    constructor(private redisService: RedisService) { }

    onApplicationBootstrap() {
        setInterval(() => {
            for(let i=0; i<10; i++) {
                this.simulateOrder();
            }
        }, 1000 * 1)
    }

    async simulateOrder() {
        const order_received: Event = {
            event_name: 'order_received',
            event_ts: Date.now(),
            event_version: 1,
            event_payload: {
                order_id: uuid.v4(),
            }
        };
        this.logger.log(`New order: ${order_received.event_payload.order_id}`);
        await this.redisService.emit(order_received)
    }
}