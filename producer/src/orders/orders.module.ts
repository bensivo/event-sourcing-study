import { Module } from '@nestjs/common';
import { RedisModule } from 'src/redis/redis.module';
import { OrdersService } from './orders.service';

@Module({
  imports: [RedisModule],
  providers: [OrdersService]
})
export class OrdersModule {}
