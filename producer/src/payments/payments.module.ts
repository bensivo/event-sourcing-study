import { Module } from '@nestjs/common';
import { RedisModule } from 'src/redis/redis.module';
import { PaymentsService } from './payments.service';

@Module({
  imports: [RedisModule],
  providers: [PaymentsService],
})
export class PaymentsModule {}
