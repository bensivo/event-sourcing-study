import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { RedisService } from 'src/redis/redis.service';
import { Event } from 'src/events/event.interface';

@Injectable()
export class PaymentsService implements OnApplicationBootstrap{
    logger = new Logger(PaymentsService.name)
    consumerId = 'payments' + Math.round(Math.random() * 1000);

    constructor(private redisService: RedisService) { }

    onApplicationBootstrap() {
        this.redisService.poll('order_received', 'payments', this.consumerId, this.onOrderReceived.bind(this));
    }

    async onOrderReceived(event: Event) {
        await wait(Math.random() * 1000);

        this.logger.log(`Payment received for order: ${event.event_payload.order_id}`);
        this.redisService.emit({
            event_name: 'payment_received',
            event_ts: Date.now(),
            event_version: 1,
            event_payload: {
                order_id: event.event_payload.order_id
            }
        })
    }
}

function wait(ms: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, ms));
}