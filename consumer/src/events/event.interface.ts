export interface Event {
    event_name: string;
    event_ts: number;
    event_version: number; // Not used in this app, but it's best practice to include a version in event serializations.
    event_payload: any;
}