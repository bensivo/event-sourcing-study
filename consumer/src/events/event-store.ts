import { Event } from "./event.interface";

/**
 * Stores events specific to a given entity type.
 * 
 * NOTE: Create a new instance of this object for each entity type, to avoid key overlap.
 */
export class EventStore {
    events: Record<string, Event[]> = {};

    pushEvent(entityId: string, event: Event) {
        if (!this.events[entityId]) {
            this.events[entityId] = [];
        }

        this.events[entityId].push(event);
    }

    /**
     * Return all events related to this entity
     */
    getEvents(entityId: string) {
        return this.events[entityId];
    }

    /**
     * Return all events related to this entity, which were emitted after the given timestamp
     * 
     * Used when loading entities from snapshots.
     */
    getEventsSince(entityId: string, ts: number) {
        return this.events[entityId].filter(e => e.event_ts > ts);
    }

    /**
     * Return all entity ids that have events in the event store.
     */
    getEntityIds() {
        return Object.keys(this.events);
    }
}