import { EventStore } from "./event-store";
import { Entity } from "./entity.interface";
import { Event } from "./event.interface";

export type EventReducer<T extends Entity> = (entity: T, event: Event) => T

export interface EventSnapshot {
    snapshotTs: number;
    snapshotJson: string;
}

export class EventView<T extends Entity> {
    /**
     * If the number of entities is relatively small, you could use an in-memory snapshot for sure.
     * 
     * If the number of entities gets larger, it will make more sense to use a DB or Redis cache for
     * serialized snapshot objects.
     */
    snapshots: Record<string, EventSnapshot> = {};

    constructor(
        private eventStore: EventStore,
        private factory: () => T,
    ) { }

    /**
     * Get an entity's state, using snapshots(if available), and events from the event store.
     */
    get(id: string): T {
        const entity = this.factory();

        const snapshot = this.snapshots[id];
        if (snapshot) {
            console.log(`Loading snapshot for object ${id}`);
            entity.fromJson(snapshot.snapshotJson);
        }

        const events = !!snapshot ?
            this.eventStore.getEventsSince(id, snapshot.snapshotTs)
            :
            this.eventStore.getEvents(id)
            ;

        for (const e of events) {
            entity.apply(e);
        }
        return entity;
    }

    /**
     * Create and store a snapshot for this entity; to reduce future processing times.
     */
    createSnapshot(id: string): EventSnapshot {
        const entity = this.get(id);
        this.snapshots[id] = {
            snapshotJson: entity.toJson(),

            /**
             * NOTE: This implementation attaches a date to the snapshot, so future readers can skip all events that ocurred before that date.
             * 
             * There are a few issues with this implementation:
             *  - Concurrency. It's possible that new events are being written in between snapshot creation and snapshot serialization. Those events may be lost, or applied twice.
             *    An alternative strategy is storing the TS of the last event used, not the TS of the snapshot creation.
             * 
             *  - Offsets. Many event streaming systems use the concept of an event 'offset' or 'index' to organize events in a stream. If your streaming platform has that behavior,
             *    it can be easier to just use offsets instead of timestamps.
             */
            snapshotTs: Date.now(), 
        };

        /**
         * Depending on your business requirements, you could decide to remove any events which ocurred before your snapshot time.
         * Only do this if you are using a persistent snapshot mechanism, and you don't care about historical data past a certain date.
         * 
         * this.eventStore.deleteEventsSince(this.snapshots[id].ts)
         */

        return this.snapshots[id];
    }
}