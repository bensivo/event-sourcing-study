import { Event } from "./event.interface";

export interface Entity {
    apply(event: Event);

    toJson(): any;
    fromJson(str: any): void; // Normally, this would be a static method, but TS can't do static methods on interfaces
}