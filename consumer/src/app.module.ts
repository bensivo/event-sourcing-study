import { Module } from '@nestjs/common';
import { OrdersModule } from './orders/orders.module';
import { RedisModule } from './redis/redis.module';
import { RunnersModule } from './runners/runners.module';

@Module({
  imports: [
    RedisModule,
    OrdersModule,
    RunnersModule,
  ],
})
export class AppModule { }
