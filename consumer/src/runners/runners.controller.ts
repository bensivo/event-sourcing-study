import { Controller, Get, Param } from '@nestjs/common';
import { RunnersService } from './runners.service';

@Controller('runners')
export class RunnersController {
    constructor(private service: RunnersService) { }

    @Get('/')
    getRunners() {
        return this.service.getRunners()
    }

    @Get('/:id')
    getRunner(@Param('id') id: string) {
        return this.service.getRunner(id)
    }
}
