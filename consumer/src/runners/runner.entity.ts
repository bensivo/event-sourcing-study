import { Event } from "src/events/event.interface";

export class RunnerEntity {
    runner_id: string = '';
    updated_at: number = 0;
    active_orders: string[] = [];
    completed_orders: string[] = [];

    _events: Event[] = [];

    apply(event: Event) {
        this._events.push(event);
        this.runner_id = event.event_payload.runner_id;
        this.updated_at = event.event_ts;

        switch (event.event_name) {
            case 'shipment_started':
                this.active_orders.push(event.event_payload.order_id)
                break;
            case 'shipment_delivered':
                this.active_orders = this.active_orders.filter(a => a !== event.event_payload.order_id);
                this.completed_orders.push(event.event_payload.order_id);
                break;
        }
    }

    toJson(): any {
        return JSON.stringify({
            runner_id: this.runner_id,
            updated_at: this.updated_at,
            active_orders: this.active_orders,
            completed_orders: this.completed_orders,
            _events: this._events,
        })
    }

    fromJson(str: any) {
        const json = JSON.parse(str);
        this.runner_id = json.runner_id;
        this.updated_at = json.updated_at;
        this.active_orders = json.active_orders;
        this.completed_orders = json.completed_orders;
        this._events = json._events;
    }
}