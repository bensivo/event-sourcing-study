import { Inject, Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { EventStore } from 'src/events/event-store';
import { EventView } from 'src/events/event-view';
import { Event } from 'src/events/event.interface';
import { RedisService } from 'src/redis/redis.service';
import { RunnerEntity } from './runner.entity';

export interface Runner {
    runner_id: string,
    updated_at: number,
    active_orders: string[],
    completed_orders: string[],
}

@Injectable()
export class RunnersService implements OnApplicationBootstrap {

    @Inject()
    private redisService: RedisService;

    private logger = new Logger(RunnersService.name);
    consumerId = 'runners-' + Math.round(Math.random() * 1000);

    eventStore: EventStore = new EventStore();
    eventView: EventView<RunnerEntity> = new EventView(this.eventStore, () => new RunnerEntity());

    async onApplicationBootstrap() {
        this.redisService.poll('shipment_started',   'runner', this.consumerId, this.handleRunnerEvent.bind(this));
        this.redisService.poll('shipment_delivered', 'runner', this.consumerId, this.handleRunnerEvent.bind(this));

        setInterval(() => {
            this.createSnapshots();
        }, 1000 * 30)
    }

    async handleRunnerEvent(event: Event) {
        this.logger.log(`New runner event: ${JSON.stringify(event)}`);
        this.eventStore.pushEvent(event.event_payload.runner_id, event)
    }

    getRunners() {
        return this.eventStore.getEntityIds()
            .map(id => this.getRunner(id));
    }

    getRunner(id: string) {
        return this.eventView.get(id);
    }

    createSnapshots() {
        for (const runner_id of this.eventStore.getEntityIds()) {
            this.eventView.createSnapshot(runner_id);
            this.logger.log(`Created snapshot for runner:${runner_id}`);
        }
    }
}
