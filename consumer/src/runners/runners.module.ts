import { Module } from '@nestjs/common';
import { RedisModule } from 'src/redis/redis.module';
import { RunnersController } from './runners.controller';
import { RunnersService } from './runners.service';

@Module({
  imports: [RedisModule],
  controllers: [RunnersController],
  providers: [RunnersService]
})
export class RunnersModule {}
