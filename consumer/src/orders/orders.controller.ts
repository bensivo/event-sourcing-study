import { Controller, Get, Param } from '@nestjs/common';
import { OrdersService } from './orders.service';

@Controller('orders')
export class OrdersController {
    constructor(private service: OrdersService) { }

    @Get('/')
    getOrders() {
        return this.service.getOrders()
    }

    @Get('/:id')
    getOrder(@Param('id') id: string) {
        return this.service.getOrder(id)
    }
}
