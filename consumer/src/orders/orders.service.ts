import { Inject, Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { EventStore } from 'src/events/event-store';
import { EventView } from 'src/events/event-view';
import { Event } from 'src/events/event.interface';
import { RedisService } from 'src/redis/redis.service';
import { OrderEntity } from './order.entity';

@Injectable()
export class OrdersService implements OnApplicationBootstrap {
    @Inject()
    private redisService: RedisService

    logger = new Logger(OrdersService.name);
    consumerId = 'orders-' + Math.round(Math.random() * 1000);

    eventStore: EventStore = new EventStore();
    eventView: EventView<OrderEntity> = new EventView(this.eventStore, () => new OrderEntity());

    async onApplicationBootstrap() {
        this.redisService.poll('order_received', 'orders', this.consumerId, this.handleOrderEvent.bind(this));
        this.redisService.poll('payment_received', 'orders', this.consumerId, this.handleOrderEvent.bind(this));
        this.redisService.poll('shipment_started', 'orders', this.consumerId, this.handleOrderEvent.bind(this));
        this.redisService.poll('shipment_delivered', 'orders', this.consumerId, this.handleOrderEvent.bind(this));

        setInterval(() => {
            this.createSnapshots();
        }, 1000 * 30)
    }

    async handleOrderEvent(event: Event) {
        this.logger.log(`New order event: ${JSON.stringify(event)}`);
        this.eventStore.pushEvent(event.event_payload.order_id, event)
    }

    getOrders(): OrderEntity[] {
        const orders = [];
        for (const id of this.eventStore.getEntityIds()) {
            console.log(id);
            orders.push(this.getOrder(id));
        }
        return orders;
    }

    getOrder(id: string): OrderEntity {
        return this.eventView.get(id);
    }

    createSnapshots() {
        // TODO: Only create snapshots for orders which have received new events since the last snapshot.
        for (const order_id of this.eventStore.getEntityIds()) {
            const snapshot = this.eventView.createSnapshot(order_id);
            this.logger.log(`Created snapshot for order:${order_id}, ts: ${snapshot.snapshotTs}`)
        }
    }
}
