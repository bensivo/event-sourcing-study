import { Module } from '@nestjs/common';
import { RedisModule } from 'src/redis/redis.module';
import { OrdersService } from './orders.service';
import { OrdersController } from './orders.controller';

@Module({
  imports: [RedisModule],
  providers: [OrdersService],
  controllers: [OrdersController]
})
export class OrdersModule {}
