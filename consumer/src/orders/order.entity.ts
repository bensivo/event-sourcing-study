import { Event } from "src/events/event.interface";

export class OrderEntity {
    order_id: string = '';
    updated_at: number = 0;
    status: string = '';
    _events: Event[] = [];

    apply(event: Event) {
        this._events.push(event);
        this.order_id = event.event_payload.order_id;
        this.updated_at = event.event_ts;

        switch (event.event_name) {
            case 'order_received':
                this.status = 'received';
                break;
            case 'payment_received':
                this.status = 'paid';
                break;
            case 'shipment_started':
                this.status = 'shipped';
                break;
            case 'shipment_delivered':
                this.status = 'delivered';
                break;
        }
    }

    toJson(): any {
        return JSON.stringify({
            order_id: this.order_id,
            updated_at: this.updated_at,
            status: this.status,
            _events: this._events,
        })
    }

    fromJson(str: any) {
        const json = JSON.parse(str);
        this.order_id = json.order_id;
        this.updated_at = json.updated_at;
        this.status = json.status;
        this._events = json._events;
    }
}