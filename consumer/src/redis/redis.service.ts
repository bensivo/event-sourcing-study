import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { Redis } from 'ioredis';
import { Event } from 'src/events/event.interface';

@Injectable()
export class RedisService implements OnApplicationBootstrap {
    logger = new Logger(RedisService.name);

    redis: Redis;

    async onApplicationBootstrap() {
        this.redis = new Redis({
            port: 6379,
            host: 'localhost',
        });
    }

    async emit(event: Event) {
        const id = await this.redis.xadd(event.event_name, '*', 'json', JSON.stringify(event));
        this.logger.debug(`Published to stream ${event.event_name}: ${id}`);
    }

    async poll(stream: string, group: string, consumer: string, callback: (event: any) => void | Promise<void>) {
        this.logger.log(`Subscribing to stream: ${stream}, group: ${group}, consumer: ${consumer}`);

        // Create stream if it doesn't exist already
        const exists = await this.redis.exists(stream);
        if (exists === 0) {
            console.log(`Creating empty stream ${stream}`);
            await this.redis.xadd(stream, 'MAXLEN', '=', '0', '*', 'key', 'value')
        } else {
            console.log(`Stream ${stream} already exists`);
        }


        // Create group if it doesn't exist already
        const info: any[] = await this.redis.xinfo('GROUPS', stream) as any[];
        const groupExists = info.some(i => i[1] === group);
        if (!groupExists) {
            console.log(`Creating group ${stream}:${group}`);
            await this.redis.xgroup('CREATE', stream, group, '0', 'MKSTREAM');
        } else {
            console.log(`Group ${stream}:${group} already exists`);
        }

        const poll = async () => {
            // NOTE: The 'BLOCK' argument is often used in consumers, but in this case, we have N parallel consumers sharing the 
            //       same redis client. Meaning blocking in 1 consumer actually stops all the other consumers too. That is why we have implemented
            //       non-blocking calls with polling. This has a downside, the interval does not slow down if we cannot process all our messages in 1 second.
            //
            //          const res = await this.redis.xreadgroup('GROUP', group, consumer, 'COUNT', '10', 'BLOCK', 1000, 'STREAMS', stream, '>')

            const res = await this.redis.xreadgroup('GROUP', group, consumer, 'COUNT', '10', 'STREAMS', stream, '>')
            if (!res) {
                return;
            }

            for (const streamRes of res) {
                const stream = streamRes[0];
                const messages = streamRes[1];

                for (const message of messages) {
                    const id = message[0];
                    const value = message[1];

                    const json = JSON.parse(value[1]);
                    await callback(json);
                    await this.redis.xack(stream, group, id); // If we wanted to, we could ack all the messages in the batch at once.
                }
            }
        }

        setInterval(poll, 1000);
    }
}
